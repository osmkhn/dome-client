﻿using System;
using System.Collections.Generic;
using DomeClient.Entities;
using DomeClient.Entities.Project;
namespace DomeClient.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Instantiating Model object with default values and serializing into XML");

            Model model = GetTestModel();
            ModelInterface modelDMI = GetTestModelInterface();
            ModelMapping modelMap = getTestModelMap();
            Project project = GetTestProject();

            string serializedModel = model.SerializeObject();
            string serializedModelInterface = modelDMI.SerializeObject();
            string serializedModelMapping = modelMap.SerializeObject();
            string serializedProject = project.SerializeObject();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Model:" + Environment.NewLine + serializedModel);
            Console.ResetColor();

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Model Interface:" +Environment.NewLine + serializedModelInterface);
            Console.ResetColor();

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Model Mapping:" +Environment.NewLine + serializedModelMapping);
            Console.ResetColor();

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Project" +Environment.NewLine + serializedProject);
            Console.ResetColor();
            
            Console.WriteLine("Press any key to deserialize back into objects");
            Console.ReadLine();

            Model reconstructedModel = Entities.Utils.Deserialize<Model>(serializedModel);
            ModelInterface reconstructedModelInterface = Entities.Utils.Deserialize<ModelInterface>(serializedModelInterface);
            ModelMapping reconstructedModelMapping = Entities.Utils.Deserialize<ModelMapping>(serializedModelMapping);
            Project reconstructedProject = Entities.Utils.Deserialize<Project>(serializedProject);

            Console.WriteLine(reconstructedModel.Name);
            Console.WriteLine(reconstructedModelInterface.Name);
            Console.WriteLine(reconstructedModelMapping.Name);
            Console.WriteLine(reconstructedProject.Name);

            Console.ReadLine();
        }

        static Model GetTestModel()
        {
            Model model = new Model { Name = "New Model 1", Type = "Simple Code Model" };
            model.ModelInformation.Version = "0.0.1";

            model.Parameters.Add(new Parameter { Name = "width" });
            model.Parameters[0].CurrentType = new ParameterType { Value = "Real" };
            ((Parameter)model.Parameters[0]).DataObjects.Add(new DataObject { Type = "Real", Quantity = new Quantity { Magnitude = 3, Unit = "No_Unit" } });

            model.Parameters.Add(new Parameter { Name = "paramstring" });
            model.Parameters[1].CurrentType = new ParameterType { Value = "String" };
            ((Parameter)model.Parameters[1]).DataObjects.Add(new DataObject { Type = "String", Value = "sdfsdf" });


            model.Relations.Add(new Relation { Name = "width", Type = "Procedural" });
            model.Relations[0].Body = new RelationBody { Body = "bodytext" };
            model.Relations[0].Parameters.Add(new Parameter { Name = "widthR1", CurrentType = new ParameterType { Value = "" }, DataObjects = new List<DataObject> { new DataObject { Quantity = new Quantity { Magnitude = 3, Unit = "NO_UNIT" }, Type = "REAL" } } });

            model.Contexts.Add(new Context { Id = "CTX_ID", Name = "CTX-NAME" });
            model.Contexts[0].ModelObjects.Add(new ContextParameter { IDModelRef = "afaefdasfdasdf", IDRef = "asdadasdads", Name = "asdadasd" });
            model.Contexts[0].ModelObjects.Add(new ContextRelation { IDModelRef = "afaefdasfdasdf", IDRef = "asdadasdads", Name = "asdadasd" });

            return model;
        }
        
        private static Project GetTestProject()
        {
            Project project = new Project { Name = "sdfdsf" };
            project.ProjectInfo.Version = "1.0.0.";
            project.Resources.Add(new Resource { Type = "ads", Name = "sdfsdf", Location = new ResourceLocation { HostName="dsafsdF", IPAddress="asdasd", Port="adfsafd" } });
            project.Resources[0].SubscribedInterfaces.Add(new SubscribedInterface { InterfaceID = "adfasfdasdf" });

            project.IntegrationModels.Add(new IntegrationModel { Name = "dafdsafdsaf" });

            return project;
        }

        private static ModelMapping getTestModelMap()
        {
            ModelMapping model = new ModelMapping { Name = "New Model 1", Type = "Simple Code Model" };
            model.InterfaceInfo.Version = "0.0.1";
            model.InterfaceInfo.DefaultInterface = string.Empty;

            model.MandI.Mappings.MappedParameters.Add(new MappedParameter { IDRef = "sdfdsf", Name = "asdasdsad" , ParameterMapped=new ReferencedParameter { IDRef = "dfsdf", IDRelationRef = "asdsad", Name = "dsafsdf" } });

            model.MandI.Parameters.Add(new Parameter { Name = "width", CurrentType = new ParameterType { Value = "Real" } });
            //model.MandI.Parameters[0].CurrentType = new ParameterType { Value = "Real" };
            ((Parameter)model.MandI.Parameters[0]).DataObjects.Add(new DataObject { Type = "Real", Quantity = new Quantity { Magnitude = 3, Unit = "No_Unit" } });

            model.LookupData.ObjectMaps.Add(new ContextParameter { IDModelRef = "sdfdsf", IDRef = "dsafsdf", Name = "dfsdf" });
            model.LookupData.ObjectMaps.Add(new ContextRelation { IDModelRef = "sdfdsf", IDRef = "dsafsdf", Name = "dfsdf", Description="dsfsdf" });

            model.LookupData.ViewObjectMaps.Add(new ReferencedParameter { Name = "dafdsf", IDRef = "adsfdsafdsaf",  Description= "adsadsasd" });

            model.LookupData.RelationObjectMaps.Add(new ReferencedParameter { Name="dsafdsaf", IDRef="adasdsad", IDRelationRef="adsfasdfad", Description="asdas"});


            return model;
        }
        
        static ModelInterface GetTestModelInterface()
        {
            ModelInterface model = new ModelInterface { Name = "New Model 1", Type = "Simple Code Model" };
            model.InterfaceInfo.Version = "0.0.1";

            model.Parameters.Add(new Parameter { Name = "width", CurrentType= new ParameterType { Value = "Real" } });
            //model.Parameters[0].CurrentType = new ParameterType { Value = "Real" };
            ((Parameter)model.Parameters[0]).DataObjects.Add(new DataObject { Type = "Real", Quantity = new Quantity { Magnitude = 3, Unit = "No_Unit" } });

            model.Relations.Add(new Relation { Name = "width", Type = "Procedural" });
            model.Relations[0].Body = new RelationBody { Body = "bodytext" };
            model.Relations[0].Parameters.Add(new Parameter { Name = "widthR1", DataObjects = new List<DataObject> { new DataObject { Quantity = new Quantity { Magnitude = 3, Unit = "NO_UNIT" }, Type = "REAL" } } });

            model.Contexts.Add(new Context { Id = "CTX_ID", Name = "CTX-NAME" });
            model.Contexts[0].ModelObjects.Add(new ContextParameter { IDModelRef = "afaefdasfdasdf", IDRef = "asdadasdads", Name = "asdadasd" });
            model.Contexts[0].ModelObjects.Add(new ContextRelation { IDModelRef = "afaefdasfdasdf", IDRef = "asdadasdads", Name = "asdadasd" });

            model.Views.Add(new View {  Name="ViewName" });
            model.Views[0].Contexts.Add(new Context { Id = "CTX_ID", Name = "CTX-NAME" });
            model.Views[0].Contexts[0].ModelObjects.Add(new ContextParameter { IDModelRef = "afaefdasfdasdf", IDRef = "asdadasdads", Name = "asdadasd" });
            model.Views[0].Contexts[0].ModelObjects.Add(new ContextRelation { IDModelRef = "afaefdasfdasdf", IDRef = "asdadasdads", Name = "asdadasd" });

            model.DirectedGraph.Nodes.Add(new Node { IDRef = "adsfsfdsadfsadf" });
            model.DirectedGraph.Arcs.Add(new ArcFrom { IdRef = "afdsadfsdf" });
            model.DirectedGraph.Arcs[0].To.IdRef = "afdafdasdsada";

            return model;
        }
        
    }
}
