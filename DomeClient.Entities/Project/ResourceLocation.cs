﻿using System.Xml.Serialization;

namespace DomeClient.Entities.Project
{
    public class ResourceLocation
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlAttribute("hostName")]
        public string HostName { get; set; }

        [XmlAttribute("ipAddress")]
        public string IPAddress { get; set; }

        [XmlAttribute("port")]
        public string Port { get; set; }
    }
}
