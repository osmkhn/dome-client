﻿

using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities.Project
{
    public class Resource
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("location")]
        public ResourceLocation Location { get; set; } = new ResourceLocation();

        [XmlArray("subscribedInterfaceIds")]
        [XmlArrayItem("ifaceId")]
        public List<SubscribedInterface> SubscribedInterfaces { get; set; } = new List<SubscribedInterface>();
    }
}
