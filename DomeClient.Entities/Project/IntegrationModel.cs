﻿using System.Xml.Serialization;

namespace DomeClient.Entities.Project
{
    public class IntegrationModel
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }
    }
}
