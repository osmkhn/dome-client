﻿
using System.Xml.Serialization;

namespace DomeClient.Entities.Project
{
    public class SubscribedInterface
    {
        [XmlText()]
        public string InterfaceID { get; set; }
    }
}
