﻿using System.Xml.Serialization;

namespace DomeClient.Entities.Project
{
    public class ProjectInfo
    {
        [XmlElement("version")]
        public string Version { get; set; }
    }
}