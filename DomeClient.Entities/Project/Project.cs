﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.Threading.Tasks;

namespace DomeClient.Entities.Project
{
    [XmlRoot("project")]
    public class Project
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("projectinfo")]
        public ProjectInfo ProjectInfo { get; set;} = new ProjectInfo();

        [XmlArray("resources")]
        [XmlArrayItem("resource")]
        public List<Resource> Resources { get; set; } = new List<Resource>();

        [XmlArray("integrationModels")]
        [XmlArrayItem("imodel")]
        public List<IntegrationModel> IntegrationModels { get; set; } = new List<IntegrationModel>();
        //integrationModels
    }
}
