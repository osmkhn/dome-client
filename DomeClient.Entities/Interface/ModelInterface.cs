﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    [XmlRoot("modelinterface")]
    public class ModelInterface
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("interfaceinfo")]
        public InterfaceInfo InterfaceInfo { get; set; } = new InterfaceInfo();

        [XmlArray("parameters")]
        [XmlArrayItem("parameter")]
        public List<Parameter> Parameters { get; set; } = new List<Parameter>();

        [XmlArray("relations")]
        [XmlArrayItem("relation")]
        public List<Relation> Relations { get; set; } = new List<Relation>();

        [XmlArray("contexts")]
        [XmlArrayItem("context")]
        public List<Context> Contexts { get; set; } = new List<Context>();

        [XmlArray("views")]
        [XmlArrayItem("view")]
        public List<View> Views { get; set; } = new List<View>();

        [XmlElement("directedGraph")]
        public DirectedGraph DirectedGraph { get; set; } = new DirectedGraph();
    }
}
