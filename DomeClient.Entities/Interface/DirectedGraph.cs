﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class DirectedGraph
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlArray("nodes")]
        [XmlArrayItem("node")]
        public List<Node> Nodes { get; set; } = new List<Node>();

        [XmlArray("arcs")]
        [XmlArrayItem("from")]
        public List<ArcFrom> Arcs { get; set; } = new List<ArcFrom>();
    }
}
