﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.Threading.Tasks;

namespace DomeClient.Entities
{
    public class InterfaceInfo
    {
        [XmlElement("version")]
        public string Version { get; set; }

        [XmlElement("defaultinterface")]
        public string DefaultInterface { get; set; }

    }
}
