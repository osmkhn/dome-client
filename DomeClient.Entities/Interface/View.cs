﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class View
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("context")]
        public List<Context> Contexts { get; set; } = new List<Context>();
    }
}
