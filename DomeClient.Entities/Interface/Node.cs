﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class Node
    {
        [XmlAttribute("idRef")]
        public string IDRef { get; set; }
    }
}
