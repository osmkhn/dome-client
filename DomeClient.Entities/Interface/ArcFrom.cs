﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class ArcFrom
    {
        [XmlAttribute("idRef")]
        public string IdRef { get; set; }

        [XmlElement("to")]
        public ArcTo To { get; set; } = new ArcTo();
    }
}
