﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class ArcTo
    {
        [XmlAttribute("idRef")]
        public string IdRef { get; set; }
    }
}
