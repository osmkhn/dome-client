﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class ParameterMapped
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("idRef")]
        public string IDRef { get; set; }

        [XmlAttribute("idModelRef")]
        public string IDModelRef { get; set; }

        [XmlAttribute("idRelationRef")]
        public string IDRelationRef { get; set; }
    }
}
