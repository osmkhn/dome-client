﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    [XmlRoot("modelinterface")]
    public class ModelMapping
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("interfaceinfo")]
        public InterfaceInfo InterfaceInfo { get; set; } = new InterfaceInfo();

        [XmlElement("mappingsandintermediates")]
        public MappingsAndIntermediates MandI { get; set; } = new MappingsAndIntermediates();

        [XmlElement("lookupdata")]
        public LookupData LookupData { get; set; } = new LookupData();
    }
}
