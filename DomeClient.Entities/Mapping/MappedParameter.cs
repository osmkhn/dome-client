﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class MappedParameter
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("idRef")]
        public string IDRef { get; set; }

        [XmlElement("parameter")]
        public ReferencedParameter ParameterMapped { get; set; } = new ReferencedParameter();
    }
}
