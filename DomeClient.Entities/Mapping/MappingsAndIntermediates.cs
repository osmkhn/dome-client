﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class MappingsAndIntermediates
    {
        [XmlElement("mappings")]
        public Mappings Mappings { get; set; } = new Mappings();

        [XmlArray("intermediateobjects")]
        [XmlArrayItem("parameter")]
        public List<Parameter> Parameters { get; set; } = new List<Parameter>();
    }
}
