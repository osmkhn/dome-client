﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class Mappings
    {
        [XmlArray("interfacemappings")]
        [XmlArrayItem("mappedParameter")]
        public List<MappedParameter> MappedParameters = new List<MappedParameter>();
    }
}
