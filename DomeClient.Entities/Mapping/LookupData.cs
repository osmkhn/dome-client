﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class LookupData
    {
        [XmlArray("objectmap")]
        [XmlArrayItem("parameter", Type = typeof(ContextParameter))]
        [XmlArrayItem("relation", Type = typeof(ContextRelation))]
        public List<ModelObject> ObjectMaps { get; set; } = new List<ModelObject>();

        [XmlArray("viewobjectmap")]
        [XmlArrayItem("parameter")]
        public List<ReferencedParameter> ViewObjectMaps { get; set; } = new List<ReferencedParameter>();

        [XmlArray("relationobjectmap")]
        [XmlArrayItem("parameter")]
        public List<ReferencedParameter> RelationObjectMaps { get; set; } = new List<ReferencedParameter>();


    }
}
