﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class Dependency
    {
        [XmlAttribute("idRef")]
        public string IDRef { get; set; }

        [XmlElement("parameter")]
        public List<ReferencedParameter> Parameters { get; set; } = new List<ReferencedParameter>();
    }
}
