﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class ReferencedParameter
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("idRef")]
        public string IDRef { get; set; }

        [XmlAttribute("idRelationRef")]
        public string IDRelationRef { get; set; }

        [XmlAttribute("idModelRef")]
        public string IDModelRef { get; set; }

        [XmlAttribute("description")]
        public string Description { get; set; }
    }
}
