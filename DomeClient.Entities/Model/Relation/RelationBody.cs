﻿using System;
using System.Xml;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class RelationBody
    {
        //public RelationBody()
        //{
        //    Body = string.Empty;
        //}

        //public RelationBody(string body)
        //{
        //    Body = body;
        //}

        [XmlIgnore]
        public string Body { get; set; }

        [XmlText]
        public XmlNode[] CDataContent
        {
            get
            {
                var dummy = new XmlDocument();
                return new XmlNode[] { dummy.CreateCDataSection(Body) };
            }
            set
            {
                if (value == null)
                {
                    Body = null;
                    return;
                }

                if (value.Length != 1)
                {
                    throw new InvalidOperationException(
                        String.Format(
                            "Invalid array length {0}", value.Length));
                }

                var node0 = value[0];

                if (node0 == null)
                {
                    throw new InvalidOperationException(
                        String.Format(
                            "Invalid node type {0}", node0.NodeType));
                }

                Body = node0.Value;
            }
        }
    }
}
