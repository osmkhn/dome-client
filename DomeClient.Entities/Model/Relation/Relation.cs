﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class Relation
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlArray("parameters")]
        //[XmlArrayItem("parameter")]
        [XmlArrayItem("parameter" /*, Type = typeof(RealParameter)*/)]
        public List<Parameter> Parameters { get; set; } = new List<Parameter>();

        [XmlElement("body")]
        public RelationBody Body { get; set; } = new RelationBody();

        [XmlArray("dependencies")]
        [XmlArrayItem("dependency")]
        public List<Dependency> Dependencies { get; set; } = new List<Dependency>();


    }
}
