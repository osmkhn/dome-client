﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class Quantity
    {
        [XmlAttribute("magnitude")]
        public decimal Magnitude { get; set; }

        [XmlAttribute("unit")]
        public string Unit { get; set; }
    }
}
