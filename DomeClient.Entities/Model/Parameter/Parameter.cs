﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    //[XmlType(TypeName = "ParameterBase")]
    public class Parameter
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("currentType")]
        public ParameterType CurrentType { get; set; } = new ParameterType();

        [XmlArray("data")]
        [XmlArrayItem("dataobject")]
        public List<DataObject> DataObjects { get; set; } = new List<DataObject>();
    }
}
