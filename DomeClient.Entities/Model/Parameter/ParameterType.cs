﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    [XmlRoot("currentType")]
    public class ParameterType
    {
        [XmlAttribute("value")]
        public string Value { get; set; }
    }
}
