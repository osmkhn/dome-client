﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class Data
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}
