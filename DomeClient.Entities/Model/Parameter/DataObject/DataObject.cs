﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class DataObject
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("quantity")]
        public Quantity Quantity { get; set; } = new Quantity();
        public bool ShouldSerializeQuantity() { return Type == "Real" || Type == "Iteration Variable"; }

        [XmlElement("increment")]
        public decimal Increment { get; set; }
        public bool ShouldSerializeIncrement() { return Type == "Iteration Variable"; }

        [XmlElement("limit")]
        public decimal Limit { get; set; }
        public bool ShouldSerializeLimit() { return Type == "Iteration Variable"; }

        [XmlText]
        public string Value { get; set; }
        public bool ShouldSerializeValue() { return Type == "String"; }

        [XmlElement("unit")]
        public string Unit { get; set; }
        public bool ShouldSerializeUnit() { return Type == "Preference"; }

        [XmlElement("initialValue")]
        public InitialValue InitialValue { get; set; }
        public bool ShouldSerializeInitialValue() { return Type == "Preference"; }

        [XmlElement("fixedSize")]
        public bool FixedSize { get; set; }
        public bool ShouldSerializeFixedSize() { return Type == "Preference"; }

        [XmlElement("data")]
        public Data Data { get; set; }
        public bool ShouldSerializeData() { return Type == "Preference"; }
    }
}
