﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class Context
    {
        public Context()
        {
            ModelObjects = new List<ModelObject>();
        }
        public Context(string id, string name, List<ModelObject> modelObjects) : this()
        {
            Id = id;
            Name = name;
            ModelObjects = modelObjects;
        }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlArray("modelobjects")]
        [XmlArrayItem("parameter", Type = typeof(ContextParameter))]
        [XmlArrayItem("relation", Type = typeof(ContextRelation))]
        public List<ModelObject> ModelObjects { get; set; }
    }
}
