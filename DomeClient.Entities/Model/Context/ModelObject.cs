﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class ModelObject
    {
        [XmlAttribute("idRef")]
        public string IDRef { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("idModelRef")]
        public string IDModelRef { get; set; }

        [XmlAttribute("description")]
        public string Description { get; set; }
    }
}
