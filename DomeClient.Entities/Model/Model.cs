﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.Threading.Tasks;

namespace DomeClient.Entities
{
    [XmlRoot("model")]
    public class Model
    {
        [XmlAttribute("id")]
        public string Id { get; set; } = System.Guid.NewGuid().ToString();

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("modelinfo")]
        public ModelInfo ModelInformation { get; set;} = new ModelInfo();

        [XmlArray("parameters")]
        [XmlArrayItem("parameter")]
        //[XmlArrayItem("parameter", Type = typeof(RealParameter))]
        public List<Parameter> Parameters { get; set; } = new List<Parameter>();

        [XmlArray("relations")]
        [XmlArrayItem("relation")]
        public List<Relation> Relations { get; set; } = new List<Relation>();

        //We don't have schema for Visualizations element yet so implementing as string
        [XmlElement("visualizations")]
        public string Visualizations { get; set; }

        [XmlArray("contexts")]
        [XmlArrayItem("context")]
        public List<Context> Contexts { get; set; } = new List<Context>();
    }
}
