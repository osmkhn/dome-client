﻿using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public class ModelInfo
    {
        [XmlElement("version")]
        public string Version { get; set; } 
    }
}
