﻿using System.IO;
using System.Xml.Serialization;

namespace DomeClient.Entities
{
    public static class Utils
    {
        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            //Done to remove unnecessary namespaces added to output XML
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize,ns);
                return textWriter.ToString();
            }
        }

        public static T Deserialize<T>(string input)
        where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
                return (T)ser.Deserialize(sr);
        }
    }
}
