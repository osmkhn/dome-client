# README #

This project builds a C# object model for "DOME Model" .dm files that are created by DOME Client and then hosted executed on DOME Server. Once this is done, it will open up the ability for developers to build and modify models programmatically and/or develop alternative front ends for DOME client (which is not exactly user friendly).

DM files look like this:

    <model type="DomeModel" id="314b1174-b698-1004-8f8b-8912c506e25c" name="math model">
        <modelinfo>
            <version>0.0.1</version>
        </modelinfo>
        <parameters>
          ...
        </parameters>
        <relations>
         ...    
        </relation>
           
        <visualizations/>
        <contexts>
           ...
        </contexts>
        <mappings>
            ...
        </mappings>
    </model>


This DOME-Client library will allow programmers to do this:

    Model mdl = new Model();
    mdl.Parameters.Add(new Parameter());

### Platform ###
C# (.NET 4.5)

### How do I get set up? ###

* Download and install Visual Studio 2015 Community Edition (free)
*  Clone the repository on your file system
*  Open the Solution and hit compile.

### Contribution guidelines ###

TODO

### Disclaimer ###
This repo is a volunteer effort to build clean, concise and approachable API for the DOME project. Copyright for initial schemas and APIs rests with DMDII and the Project DMC team.